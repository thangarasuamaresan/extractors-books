package bookpreviewextractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ExternalUtils {
    private static Log log = LogFactory.getLog(ExternalUtils.class);

    // ----------------------------------------------------------------------
    // Run external process
    // ----------------------------------------------------------------------
    public static String runProcess(List<String> cmd) throws IOException {
        return runProcess(cmd.toArray(new String[cmd.size()]));
    }

    public static String runProcess(String... cmd) throws IOException {
        // find executable
        cmd[0] = getExecutable(cmd[0]);

        StringBuilder sb = new StringBuilder();
        for (String s : cmd ) {
            sb.append(s);
            sb.append(" "); //$NON-NLS-1$
        }
        log.info(String.format("Running process %s.", sb.toString()));

        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.redirectErrorStream(true);
        Process p = pb.start();

        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        StringBuilder stdout = new StringBuilder();
        while (isRunning(p)) {
            while ((line = br.readLine()) != null) {
                log.debug(line);
                stdout.append(line);
                stdout.append("\n"); //$NON-NLS-1$
            }
        }
        while ((line = br.readLine()) != null) {
            log.debug(line);
            stdout.append(line);
            stdout.append("\n"); //$NON-NLS-1$
        }

        log.debug(String.format("Finished process (%d).", p.exitValue()));

        br.close();
        return stdout.toString();
    }

    public static boolean isRunning(Process p) {
        try {
            p.exitValue();
            return false;
        } catch (IllegalThreadStateException e) {
            return true;
        }
    }

    public static String getExecutable(String executable) throws IOException {
        String[] extensions;
        if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1) { //$NON-NLS-1$ //$NON-NLS-2$
            extensions = new String[] { ".exe", ".com", ".bat" }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        } else {
            extensions = new String[] { "" }; //$NON-NLS-1$
        }

        // check system path
        for (String path : System.getenv("PATH").split(File.pathSeparator) ) { //$NON-NLS-1$
            for (String ext : extensions ) {
                File exe = new File(path, executable + ext);
                if (exe.exists()) {
                    return exe.getAbsolutePath();
                }
            }
        }

        // check current folder
        for (String ext : extensions ) {
            File exe = new File(executable + ext);
            if (exe.exists()) {
                return exe.getAbsolutePath();
            }
        }

        // nothing found
        throw (new IOException(String.format("Could not find '%s' to execute.", executable)));
    }

}
