package bookpreviewextractor;

import imagepreviewpyramidextractor.ImagePreviewPyramidExtractorService;

import java.io.IOException;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;
import metadata.FileMetadata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class BookPreviewExtractor extends Extractor {

	private static final String QUEUE_NAME = "medici_book";
	private static final String EXCHANGE_NAME = "medici";
	private static final String CONSUMER_TAG = "medici_book";
	private static final boolean DURABLE = true;
	private static final boolean EXCLUSIVE = false;
	private static final boolean AUTO_DELETE = false;
	
	private static String serverAddr = "";
	private static String messageReceived = "";
	
	private static Log log = LogFactory.getLog(BookPreviewExtractor.class);
	
	public static void main(String[] args)  {
        
		if (args.length < 5){ 
			System.out.println("Input RabbitMQ server address, followed by RabbitMQ username, RabbitMQ password, "
								+ "maximum size to downscale book page images to before extracting preview(in megabytes) and Medici REST API key.");
			return; 
		}   
		
            serverAddr = args[0];
            BookPreviewExtractor.setPlayserverKey(args[4]);
            //All page images will be viewed using zoom.it for now
            ImagePreviewPyramidExtractorService.setMinSize(0);
            ImagePreviewPyramidExtractorService.setMaxSize(Integer.parseInt(args[3]));
            new BookPreviewExtractor().startExtractor(args[1], args[2]);	        
    }
	
	 protected void startExtractor(String rabbitMQUsername, String rabbitMQpassword) {
		 try{			 
				//Open channel and declare exchange and consumer
				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost(serverAddr);
				factory.setUsername(rabbitMQUsername);
				factory.setPassword(rabbitMQpassword);
				Connection connection = factory.newConnection();
		    
				final Channel channel = connection.createChannel();
				channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
		
				channel.queueDeclare(QUEUE_NAME,DURABLE,EXCLUSIVE,AUTO_DELETE,null);
				channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.file.application.zip.#");
				channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.file.application.x-zip-compressed.#");
				
				this.channel = channel;

				// create listener
		        channel.basicConsume(QUEUE_NAME, false, CONSUMER_TAG, new DefaultConsumer(channel) {
		            @Override
		            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		                messageReceived = new String(body);
		                long deliveryTag = envelope.getDeliveryTag();
		                // (process the message components here ...)
		                System.out.println(" {x} Received '" + messageReceived + "'");
		                
		                replyProps = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();
						replyTo = properties.getReplyTo();
		                
			            processMessageReceived();
			            System.out.println(" [x] Done");
		                channel.basicAck(deliveryTag, false);
		            }
		        });
				
		        // start listening
		        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		        while (true) {
		           Thread.sleep(1000);
		        }
			}
	        catch(Exception e){
	        	e.printStackTrace();
	            System.exit(1);
	        }		 
	    }
	 
	 protected void processMessageReceived(){
	    	try{
	    		try {	    			
	    			BookPreviewExtractorService extrServ = new BookPreviewExtractorService(this);
	    			jobReceived = getRepresentation(messageReceived, ExtractionJob.class);
	    			
	    			if(jobReceived.isFlagSet("nopreviews")){
						log.info("Book is not to be previewed. Aborted processing of book.");
	    				return;
					}
	    			
	    			BookMetaData bookMetaData = extrServ.processJob(jobReceived);
	    			
	    			log.info("Book extraction and association complete. Returning Book file metadata.");

	    	        String JSONString = mapper.writeValueAsString(new FileMetadata(bookMetaData.getName(), bookMetaData.getLength()));
	    	        
	    	        associatePreview(bookMetaData.getId(),JSONString, log);
	    	        
	    	        sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
	    	        	    			
				} catch (Exception ioe) {
					log.error("Could not finish extraction job.", ioe);
					sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "Could not finish extraction job.", log);
					sendStatus(jobReceived.getId(), this.getClass().getSimpleName(), "DONE.", log);
				}        
	    	}catch(Exception e){
	    		e.printStackTrace();
	            System.exit(1);
	    	}
	    } 
}














