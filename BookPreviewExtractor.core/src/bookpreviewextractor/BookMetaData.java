package bookpreviewextractor;

public class BookMetaData {

	private final String id;
	private final String name;
	private final String length;
	private final String page;
	
	public BookMetaData(String id, String name, String length, String page) {
		super();
		this.id = id;
		this.name = name;
		this.length = length;
		this.page = page;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLength() {
		return length;
	}
	
	public String getPage() {
		return page;
	}
	
	
}
