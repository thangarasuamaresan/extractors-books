package bookpreviewextractor;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.imageio.ImageIO;


class Border {
	int left, right, top, bottom;
}

class LineElem {
	int begin, middle, end;
}

class InpaintFile {
	File text = null;
	File image = null;
	File text_image = null;
}

// Pair of key and x,y position
class Pair implements Comparable {
	public double key;
	int x, y;
	public Pair(double key, int x, int y) {
	    this.key = key;
	    this.x = x;
	    this.y = y;
	}
	@Override
	public int compareTo(Object o) {
		Pair p = (Pair)o;
	    if(this.key>p.key) {
	        return -1;
	    }
	    else if(this.key<p.key) {
	        return 1;
	    }
	    return 0;
	}
}

public class InpaintUtils {
	private static int w,h;
	private static int[][] pixelmap;			//Pixel map from image
	private static int[][] pixelmarkmap;		//Marked pixel map for finding text
	private static int[][] inpaintmap;			//Marked pixel map for inpainting algorithm
	private static int mark_width=10;			//Extend region width around detected edge
	private static int side=25;
	private static int[] col_pixel_count;		//Number of text pixel by column
	private static int[] row_pixel_count;		//Number of text pixel by row
	private static Border border;				//95% image border
	private static ArrayList<LineElem> line;	//Detected lines
	private static ArrayList<String> text = new ArrayList<String>(); //Given transcribed text
	private static double[][] confident;		//Pixel confident term
	private static double[][] data;				//Pixel data term
	private static double[][][] gradient;		//Pixel gradient
	private static double[][][] normal;			//Pixel normal vector
	
	public static ArrayList<InpaintFile> extractZip(File tempfile) throws Exception {
		byte[] buf = new byte[1024];
		ArrayList<InpaintFile> file = new ArrayList<InpaintFile>();
		ArrayList<String> filetag = new ArrayList<String>();
		InpaintFile newfile = new InpaintFile();
        newfile.image = File.createTempFile("image", ".jpg");
        newfile.text = File.createTempFile("text", ".txt");
        ZipInputStream zstream = new ZipInputStream(new FileInputStream(tempfile));
        ZipEntry zentry = zstream.getNextEntry();

        while (zentry != null) {
            String entry_name[] = zentry.getName().split("\\."); // Name and Extension
            entry_name[1] = "."+entry_name[1]; // Add . back to filetype
            FileOutputStream foutstream = null;
            if(!filetag.contains(entry_name[0])) {
            	filetag.add(entry_name[0]);
            	file.add(new InpaintFile());
            }
            
            int fileid = filetag.indexOf(entry_name[0]);
            
            if(entry_name[1].equals(".jpg")) {
            	file.get(fileid).image = File.createTempFile(entry_name[0], entry_name[1]);
            	foutstream = new FileOutputStream(file.get(fileid).image);
            }
            else if(entry_name[1].equals(".txt")) {
            	file.get(fileid).text = File.createTempFile(entry_name[0], entry_name[1]);
            	foutstream = new FileOutputStream(file.get(fileid).text);
            }
            
            else {
            	// Garbage
            	continue;
            }
            
            int n;
            while ((n = zstream.read(buf, 0, 1024)) > -1)
                foutstream.write(buf, 0, n);

            foutstream.close(); 
            zstream.closeEntry();
            
            //next file
            zentry = zstream.getNextEntry();
        }
 
        zstream.close();
        return file;
	}
	
	/* Return and average non-inpainted 
	 */
	public static int[] inpaint(InpaintFile file) throws IOException, InterruptedException {
		//Reading image file
		BufferedImage img = ImageIO.read(file.image);
		w = img.getWidth();
		h = img.getHeight();
		row_pixel_count = new int[h];
		col_pixel_count = new int[w];
		
		//Reading text file
		BufferedReader br = new BufferedReader(new FileReader(file.text));
		String str;
		text.clear();
		while ((str = br.readLine()) != null)   {
			text.add(str);
		}
		br.close();
		
		//Counting pixel
		int avgColor[] = new int[3];
		avgColor = preprocess(img);
		
		//Remove text
		removeText(img);
		
		//Overlay text on the removed image
		img = overlayText(img,text);
		
		file.text_image = File.createTempFile("text", ".jpg");
		ImageIO.write(img, "jpg", file.text_image);
		return avgColor;
	}
	
	private static int[] preprocess(BufferedImage img) throws IOException, InterruptedException {
		int pixels[] = new int[w*h];
		//WritableRaster raster = img.getRaster();
        pixelmap = new int[w][h];
        pixelmarkmap = new int[w][h];
        inpaintmap = new int[w][h];
        
        //Color triple for debug
        int color[] = {0,0,0};
        int white[] = {255,255,255};
        int black[] = {0,0,0};
        int red[] = {255,0,0};
        int green[] = {0,255,0};
        int blue[] = {0,0,127};
        int aqua[] = {0,255,255};
        int yellow[] = {255,255,0};

        //Grab pixel information to array
        PixelGrabber pg = new PixelGrabber(img, 0, 0, w, h, pixels, 0, w);
		pg.grabPixels();
        for (int y=0; y<h; y++) {
            for (int x=0; x<w; x++) {
                pixelmap[x][y] = pixels [y*w+x];
                //System.out.println(i+"/"+h+","+j+"/"+w);
            }
        }
        
        //Finding edge and get average of non-inpainted color.
        int avgColor[] = new int[3];
        avgColor = edgeDetection();
        
        //Expanding region around detected text (edge) by specified mark_width
        markTextRegion();
        
        //DEBUG - Color text region with white color
        /*for(int y=0; y<h; y++) {
        	for(int x=0; x<w; x++) {
        		if(inpaintmap[x][y] > 0)
        			raster.setPixel(x, y, white);
        	}
        }
        */
        
        
        //Counting pixel by row and column
        countPixel();
        
        //Crop page to about 95% of data
        border = pageCrop(0,w,0,h);
        
        //Detect text line
        line = findTextLine();
        
        side = (int) Math.sqrt((w+h)/5);
        //(border.bottom-border.top)/text.size()/5;
        System.out.println("Pattern size  "+side);
        
        //DEBUG - Mark text line
        /*int l = border.left + 5;
        System.out.println(line.size());
        for(LineElem each_line : line) {
        	for(int i=each_line.begin; i<each_line.end; i++) {
        		for(int x=0; x<20; x++)
        			raster.setPixel(l+x, i, aqua);
        	}
        	l+= 25;
        }*/
        
        //DEBUG - Mark page border
        /*for(int y=0; y<h;y++) {
        	raster.setPixel(border.left, y, blue);
        	raster.setPixel(border.right-1, y, blue);
        }
        for(int x=0; x<w;x++) {
        	raster.setPixel(x, border.top, blue);
        	raster.setPixel(x, border.bottom-1, blue);
        }*/
        
        return avgColor;
        
	}
	
	//Edge detection by Laplacian filter 
	private static int[] edgeDetection() {
		int totalPixel = 0;
		int avgColor[] = new int[3];
		int color[] = new int[3];
        for(int y=1; y<h-1; y++) {
        	for(int x=1; x<w-1; x++) {
        		//Laplacian filter
        		color[0] = 0;
        		color[1] = 0;
        		color[2] = 0;
        		for(int j=-1; j<=1; j++) {
        			for(int i=-1; i<=1; i++) {
        				//System.out.println((x+i)+","+(y+j));
        				color[0] += 0xff & (pixelmap[x+i][y+j] >> 16);
                        color[1] += 0xff & (pixelmap[x+i][y+j] >> 8);
                        color[2] += 0xff & (pixelmap[x+i][y+j]);
        			}
        		}
        		color[0] -= 8*(0xff & (pixelmap[x][y] >> 16));
                color[1] -= 8*(0xff & (pixelmap[x][y] >> 8));
                color[2] -= 8*(0xff & (pixelmap[x][y]));
                
                // Euclidian Different
                int square_error = color[0]*color[0]+color[1]*color[1]+color[2]*color[2];
                if(square_error > 3*127*127) {
                	inpaintmap[x][y] = 0;
                	avgColor[0] += 0xff & (pixelmap[x][y] >> 16);
                	avgColor[1] += 0xff & (pixelmap[x][y] >> 8);
                	avgColor[2] += 0xff & (pixelmap[x][y]);
                	totalPixel ++;
                }
                else
                	inpaintmap[x][y] = mark_width;
        		if(square_error > 3*32*32) {
        			//raster.setPixel(x, y, white);
        			pixelmarkmap[x][y] = 0;
        		}
        		else {
        			//raster.setPixel(x, y, black);
        			pixelmarkmap[x][y] = mark_width;
        		}
        	}
        }
        avgColor[0] /= totalPixel;
        avgColor[1] /= totalPixel;
        avgColor[2] /= totalPixel;
        return avgColor;
	}
	
	//For all pixel expand from the detect pixel by mark_width pixel
	private static void markTextRegion() {
		for(int y=0; y<h; y++) {
			for(int x=0; x<w; x++) {
				if(inpaintmap[x][y] != 0) {
					markAroundText(x,y,inpaintmap[x][y]);
				}
			}
		}
	}
	
	//For each pixel expand from the detect pixel by mark_width pixel
	private static void markAroundText(int x, int y, int depth) {
		if(x>=0 && x<w && y>=0 && y<h) {
			if(depth >= inpaintmap[x][y]) {
				inpaintmap[x][y] = depth;
				markAroundText(x-1,y,depth-1);
				markAroundText(x+1,y,depth-1);
				markAroundText(x,y-1,depth-1);
				markAroundText(x,y+1,depth-1);
			}
		}
	}
	
	private static void countPixel() {
        for(int y=0; y<h; y++) {
        	for(int x=0; x<w; x++) {
        		if(pixelmarkmap[x][y] == mark_width) {
        			col_pixel_count[x]++;
        			row_pixel_count[y]++;
        		}
        	}
        }
	}
	
	private static Border pageCrop(int left, int right, int top, int bottom) {
		int pixel_count = 0;
        Border border = new Border();
        
        //Counting total pixel between top and bottom border
        for(int y=top; y<bottom; y++) {
        	pixel_count += row_pixel_count[y];
        }
        
        //Cropping inside pixel at 95% level
        int v_pixel_cumul = col_pixel_count[left];
        for(int x=left+1; x<right; x++) {
        	v_pixel_cumul += col_pixel_count[x];
        	if(v_pixel_cumul < 0.025*pixel_count)
        		border.left = x; //left
        	else if(v_pixel_cumul >= 0.975*pixel_count) {
        		border.right = x; //right
        		break;
        	}
        }
        int h_pixel_cumul = row_pixel_count[top];
        for(int y=top+1; y<bottom; y++) {
        	h_pixel_cumul += row_pixel_count[y];
        	if(h_pixel_cumul < 0.025*pixel_count)
        		border.top = y; //top
        	else if(h_pixel_cumul >= 0.975*pixel_count) {
        		border.bottom = y; //bottom
        		break;
        	}
        }
        
        //Tolerance is 5% 
        int tolerance = (int) (0.05*(right-left));
        if(tolerance > (int) (0.05*(bottom-top)))
        		tolerance = (int) (0.05*(bottom-top));

        //Adding tolerance to each sides
        border.left -= tolerance;
        border.right += tolerance;
        border.top -= tolerance;
        border.bottom += tolerance;
        
        //Cannot go over original boundary
        if(border.left<left)	border.left = left;
        if(border.right>right)	border.right = right;
        if(border.top<top)		border.top = top;
        if(border.bottom>bottom)border.bottom = bottom;
        
        System.out.println("Image size    width:"+w+ "height:"+h);
        System.out.println("Border        left:"+border.left+" right:"+border.right+" top:"+border.top+" bottom:"+border.bottom);
        
        //Repeat until stable
        /*if((border.left-left)>tolerance || (right-border.right)>tolerance || (border.top-top)>tolerance || (bottom-border.bottom)>tolerance) {
        	border = pageCrop(border.left,border.right,border.top,border.bottom);
        }*/
        
        return border;
	}
	
	private static ArrayList<LineElem> findTextLine() {
		//Score each line for distinguish between text and space
		int score_black = 15, score_red = 10, score_white = 2, score_green = -1;
		
		int page_h = border.bottom - border.top;
        int row_pixel_max[] = new int[page_h];
        int row_pixel_max_index = (int) (0.9*page_h);
        for(int y=0; y<page_h; y++) {
        	row_pixel_max[y] = row_pixel_count[border.top+y];
        }
        Arrays.sort(row_pixel_max);
        
        int[] row_score = new int[h];
        
        for(int y=0; y<h; y++) {
        	if(row_pixel_count[y] < 1) { //0.001*row_pixel_max[row_pixel_max_index] ) {
        		row_score[y] = score_black;
        		/*for(int x=0; x<w; x++) {
	    			if(pixelmarkmap[x][y] == 0)
	    				raster.setPixel(x, y, black);
	        	}*/
        	}
        	else if(row_pixel_count[y] < 0.1*row_pixel_max[row_pixel_max_index] ) {
	        	row_score[y] = score_red;
	    		/*for(int x=0; x<w; x++) {
	    			if(pixelmarkmap[x][y] == 0)
	    				raster.setPixel(x, y, red);
	        	}*/
	    	}
	        else if(row_pixel_count[y] > 0.5*row_pixel_max[row_pixel_max_index] ) {
	        	row_score[y] = score_green;
	    		/*for(int x=0; x<w; x++) {
	    			if(pixelmarkmap[x][y] == 0)
	    				raster.setPixel(x, y, green);
	        	}*/
	    	}
	        else
	        	row_score[y] = score_white;
        }
        
    	ArrayList<LineElem> line = new ArrayList<LineElem>();
		int score = 0, score_limit = 0;
		int black_line = 0;
		
		//int ytop, ymiddle, ybottom;
		for(int y=0; y<h; y++) {
			//Find middle and bottom part.
			if(row_score[y] != score_red) {
				//System.out.println(y);
				//ytop = ymiddle = y;
				LineElem new_line = new LineElem();
				new_line.begin = new_line.middle = y;
				
				black_line = 0;
				do {
					score += row_score[y];
					score_limit += score_red;
					if(row_score[y] == score_black)
						black_line++;
					y++;
					
				} while(score <= score_limit/3 && y<h && black_line <= 10);
				
				// If it is too small, ignore.
				if(y-new_line.middle>= 10) {
					//Find top part
					new_line.end = y;
					
					black_line = 0;
					while(score <= score_limit/2 && new_line.begin>0 && black_line <= 10) {
						new_line.begin--;
						score += row_score[new_line.begin];
						score_limit += score_red;
						if(row_score[new_line.begin] == score_black)
							black_line++;
					}
					
					
					// If it overlap with the previous one, merge them. 
					if(line.size() != 0) {
						if(new_line.begin < line.get(line.size()-1).end) {
							new_line.begin = line.get(line.size()-1).begin;
							line.remove(line.size()-1);
						}/*
						else if(ytop < line.get(line.size()-1)[2])
							ytop = line.get(line.size()-1)[2]+5;*/
					}
					
					//LineElem new_line = {ytop, ymiddle, ybottom};
					
					line.add(new_line); 
					//System.out.println(line.size()+"="+ytop+"|"+ymiddle+"|"+ybottom);
				}
				score = 0;
				score_limit = 0;
			}
		}
		return line;
	}
	
	//Old version of inpaint algorithm fast!
	private static void removeTextOrig(BufferedImage img) {
		int color[] = new int[3];
		int patch_color[] = new int[3];
		int black[] = {0,0,0};
		int white[] = {255,255,255};
		double new_color[] = new double[3];
		
		WritableRaster raster = img.getRaster();
		
		int edge[][] = new int[w][h];
		//int side = 25;//mark_width;
		int best_patch_x = 0, best_patch_y = 0, best_patch_error;
		//Edge detection
		for(int y=side/2; y<h-side/2; y++) {
			System.out.println("Line "+y+" from "+h);
			for(int x=side/2; x<w-side/2; x++) {
				if(inpaintmap[x][y] != 0)
					if(inpaintmap[x-1][y]*inpaintmap[x+1][y]*inpaintmap[x][y-1]*inpaintmap[x][y+1] == 0)
						if(inpaintmap[x-1][y]+inpaintmap[x+1][y]+inpaintmap[x][y-1]+inpaintmap[x][y+1] != 0)
							edge[x][y] = 1;
						
				
				
				//Simple inpaint
				if(edge[x][y] == 1) {
					
					
					int left = x-10*side, right = x+10*side;
					int top = y-10*side, bottom = y+10*side;
					if(left < side/2) left = side/2;
					if(right > w-side/2) right = w-side/2;
					if(top < side/2) top = side/2;
					if(bottom > h-side/2) bottom = h-side/2;
					
					best_patch_error = -1;
					
					for(int j=top; j<bottom; j++) {
						for(int i=left; i<right; i++) {
							if(inpaintmap[i][j] == 0) {
								int patch_error = 0;
								for(int b=-side/2; b<side/2; b++) {
									for(int a=-side/2; a<side/2; a++) {
										if(inpaintmap[x+a][y+b] == 0 && inpaintmap[i+a][j+b] == 0) {
											raster.getPixel(x+a, y+b, color);
											raster.getPixel(i+a, j+b, patch_color);
											patch_error += squareError(color, patch_color);
											
											if(best_patch_error != -1 && best_patch_error < patch_error) {
												patch_error = -1;
												break;
											}
										}
									}
								}
								
								if(patch_error != -1) {
									best_patch_error = patch_error;
									best_patch_x = i;
									best_patch_y = j;
								}
							}
						}
					}
					
					for(int b=-side/2; b<side/2; b++) {
						for(int a=-side/2; a<side/2; a++) {
							if(inpaintmap[x+a][y+b] != 0 && inpaintmap[best_patch_x+a][best_patch_y+b] == 0) {
								inpaintmap[x+a][y+b] = 0;
								edge[x+a][y+b] = 0;
								raster.getPixel(best_patch_x+a, best_patch_y+b, patch_color);
								raster.setPixel(x+a, y+b, patch_color);
							}
						}
					}
					
					//System.out.println("Inpaint "+x+","+y+" with "+best_patch_x+","+best_patch_y);
					
					//raster.setPixel(x, y, black);
				}
				
				/*if(inpaintmap[x][y] == 1) {
					for(int k=0;k<3;k++) {
						new_color[k] = 0;
					}
					for(int i=-1; i<=1; i++) {
						for(int j=-1; j<=1; j++) {
							raster.getPixel(x+i, y+j, color);
							if(i!=0 || j!=0) {
								for(int k=0;k<3;k++) {
									new_color[k] += 0.125*color[k];
								}
			                }
							raster.setPixel(x+i, y+j, white);
						}
					}
				}*/
			}
		}
	
		/*for(int y=side/2; y<h-side/2; y++) {
			for(int x=side/2; x<w-side/2; x++) {
			}
		}*/
	}
	
	private static double dotProduct(double[] gradient2, double[] normal2) {
		return Math.abs(gradient2[0]*normal2[0] + gradient2[1]*normal2[1]);
	}
	
	//Calculate data term of each pixel
	private static double calData(int x, int y) {
		return 10*dotProduct(gradient[x][y],normal[x][y]);
	}
	
	//Calculate confident term of each pixel
	private static double calConfident(int x, int y) {
		//int side = mark_width;
		double c = 0;
		
		//Find patch size
		int ymin = y-side/2, ymax = y+side/2;
		int xmin = x-side/2, xmax = x+side/2;
		if(ymin < 0) ymin = 0;
		if(ymax > h) ymax = h;
		if(xmin < 0) xmin = 0;
		if(xmax > w) xmax = w;
		
		//Sum all confident term in patch
		for(int b=ymin; b<ymax; b++) {
			for(int a=xmin; a<xmax; a++) {
				if(inpaintmap[a][b] == 0)
					c += confident[a][b];
			}
		}
		
		//Average
		return c/(ymax-ymin)/(xmax-xmin);
	}
	
	private static void findGrad(int x, int y) {
		//int side = mark_width;
		int line_length[][] = new int[3][3];
		int line_x=0, line_y=0;
		
		//Find patch size
		int ymin = y-side/2, ymax = y+side/2;
		int xmin = x-side/2, xmax = x+side/2;
		if(ymin < 0) ymin = 0;
		if(ymax > h) ymax = h;
		if(xmin < 0) xmin = 0;
		if(xmax > w) xmax = w;
		
		int jmin = -1, jmax = 1;
		int imin = -1, imax = 1;
		if(y+jmin < 0) jmin = 0;
		if(y+jmax > h) jmax = 0;
		if(x+imin < 0) imin = 0;
		if(x+imax > w) imax = 0;
		
		//Sum all confident term in patch		
		for(int j=jmin;j<jmax;j++) {
			for(int i=imin;i<imax;i++) {
				if(i!=0 || j!=0) {
					int xcur=x+i, ycur=y+j;
					while(inpaintmap[xcur][ycur] == 0) {
						int xnext = xcur + (int) ((i)*Math.abs(gradient[xcur][ycur][0]));
						int ynext = ycur + (int) ((j)*Math.abs(gradient[xcur][ycur][1]));
						if(xnext<0 && xnext>=w && ynext<0 && ynext>=h )
							break;
						//System.out.println("x"+xcur+" y"+ycur+" x1"+xnext+" y1"+ynext+" dx"+gradient[xcur][ycur][0]+" dy"+gradient[xcur][ycur][1]);
						if(xnext==xcur && ynext==ycur)
							break;
						if(dotProduct(gradient[xcur][ycur],gradient[xnext][ynext]) < 0.5) {
							line_length[i+1][j+1]++;
							xcur = xnext;
							ycur = ynext;
						}
						else
							break;
					}
					if(line_length[i+1][j+1] > line_length[line_x+1][line_y+1])
					{
						line_x = i;
						line_y = j;
					}
				}
			}
		}
		
		gradient[x][y] = gradient[x+line_x][y+line_y];
	}
	
	private static void removeText(BufferedImage img) {
		int color[] = new int[3];
		int patch_color[] = new int[3];
		int black[] = {0,0,0};
		int white[] = {255,255,255};
		double new_color[] = new double[3];
		
		WritableRaster raster = img.getRaster();
		
		int edge[][] = new int[w][h];
		confident = new double[w][h];
		data = new double[w][h];
		gradient = new double[w][h][2];
		normal = new double[w][h][2];
		
		PriorityQueue<Pair> PQ = new PriorityQueue<Pair>(w*h);
		//int side = 25;//mark_width;
		int best_patch_x = 0, best_patch_y = 0, best_patch_error;
		
		//Initialize confident term
		for(int y=0; y<h; y++) {
			for(int x=0; x<w; x++) {
				if(inpaintmap[x][y] == 0)
					confident[x][y] = 1;
				else
					confident[x][y] = 0;
			}
		}
		
		System.out.print("Find gradient [");
		int y_counter = 0;
		for(int y=side/2; y<h-side/2; y++) {
			while(100*y/(h-side) > y_counter) {
				y_counter++;
				if(y_counter % 10 == 0)
					System.out.print(y_counter/10);
				else
					System.out.print('.');
			}
			for(int x=side/2; x<w-side/2; x++) {
				//Edge detection
				if(inpaintmap[x][y] != 0) {
					if(inpaintmap[x-1][y]*inpaintmap[x+1][y]*inpaintmap[x][y-1]*inpaintmap[x][y+1] == 0)
						if(inpaintmap[x-1][y]+inpaintmap[x+1][y]+inpaintmap[x][y-1]+inpaintmap[x][y+1] != 0) {
							edge[x][y] = 1;
							//PQ.add(new Pair(calConfident(x,y),x,y));
						}
				}
				else {
					//Gradient
					double ch[] = new double[3];
					double cl[] = new double[3];
					double gradx, grady;
					raster.getPixel(x+1, y, ch);
					raster.getPixel(x-1, y, cl);
					gradx = (ch[0]+ch[1]+ch[2]-cl[0]-cl[1]-cl[2])/255/3;
					/*//Normalize
					if(gradx < 0)
						gradx = -1;
					else if(gradx > 0)
						gradx = 1;*/
					raster.getPixel(x, y+1, ch);
					raster.getPixel(x, y-1, cl);
					grady = (ch[0]+ch[1]+ch[2]-cl[0]-cl[1]-cl[2])/255/3;
					/*//Normalize
					if(grady < 0)
						grady = -1;
					else if(grady > 0)
						grady = 1;*/
					//Perpendicular to gradient
					gradient[x][y][0] = grady;
					gradient[x][y][1] = -gradx;
					//System.out.println("x"+x+" y"+y+" dx"+gradient[x][y][0]+" dy"+gradient[x][y][1]);
				}
			}
		}
		System.out.println("] DONE");
		
		//Find normal vector to each edge
		for(int y=side/2; y<h-side/2; y++) {
			for(int x=side/2; x<w-side/2; x++) {
				if(edge[x][y] == 1) {
					normal[x][y][0] = edge[x+1][y] - edge[x-1][y];
					normal[x][y][1] = edge[x][y+1] - edge[x][y-1];
					findGrad(x,y);
				}
			}
		}
		
		int pq_counter = 0;
		int pq_size = 0;
		
		//Prioritize queue by the product of confident term and data term
		for(int y=side/2; y<h-side/2; y++) {
			for(int x=side/2; x<w-side/2; x++) {
				if(edge[x][y] == 1) {
					//Debug
					/*color[0] = 0;
					color[1] = 0;
					color[2] = 0;
					if(normal[x][y][0] > 0)
						color[0] = 255;
					else if(normal[x][y][0] < 0)
						color[2] = 255;*/
					
					/*if(normal[x][y][1] > 0)
						color[1] = 255;*/
					
					//int red[] = {255,0,0};
					//raster.setPixel(x,y,color);
					data[x][y] = calData(x,y);
					if(data[x][y] > 0)
						System.out.println(data[x][y]);
					confident[x][y] = calConfident(x,y);
					PQ.add(new Pair(confident[x][y]+data[x][y],x,y));
					pq_size++;
				}
			}
		}
		
		
		System.out.print("Inpaint       [");
		Pair p;
		int time = 0;
		//Pop element from priority queue
		while((p = PQ.poll()) != null && time < 1000000) {
				time++;
				double percent = 100*time/pq_size; 
				double percent_weight = 2-percent/100; 
				while(percent_weight*percent > pq_counter && pq_counter < 100) {
					pq_counter++;
					if(pq_counter % 10 == 0)
						System.out.print(pq_counter/10);
					else
						System.out.print('.');
				}
				int x = p.x;
				int y = p.y;

				if(x>=side/2 && x<w-side/2 && y>=side/2 && y<h-side/2 && edge[x][y] == 1 && inpaintmap[x][y] != 0) {
					//System.out.print((++time)+" Inpaint "+x+","+y+" key="+p.key+" c="+confident[x][y]+" d="+data[x][y]);
					
					int left = x-50-2*side, right = x+50+2*side;
					int top = y-50-2*side, bottom = y+50+2*side;
					if(left < side/2) left = side/2;
					if(right > w-side/2) right = w-side/2;
					if(top < side/2) top = side/2;
					if(bottom > h-side/2) bottom = h-side/2;
					
					best_patch_error = -1;
					
					for(int j=top; j<bottom; j++) {
						for(int i=left; i<right; i++) {
							if(inpaintmap[i][j] == 0) {
								int patch_error = 0;
								for(int b=-side/2; b<side/2 && patch_error!=-1; b++) {
									for(int a=-side/2; a<side/2 && patch_error!=-1; a++) {
										if(inpaintmap[i+a][j+b] != 0)
											patch_error = -1; //complete patch only
										else if(inpaintmap[x+a][y+b] == 0 && inpaintmap[i+a][j+b] == 0) {
											raster.getPixel(x+a, y+b, color);
											raster.getPixel(i+a, j+b, patch_color);
											patch_error += squareError(color, patch_color);
											
											if(best_patch_error != -1 && best_patch_error < patch_error) {
												patch_error = -1;
											}
										}
									}
								}
								
								if(patch_error != -1) {
									best_patch_error = patch_error;
									best_patch_x = i;
									best_patch_y = j;
								}
							}
						}
					}
					
					if(best_patch_error == -1) {
						System.out.println(" Error : Cannot find matching patch with in region l:"+left+" r:"+right+" t:"+top+" b:"+bottom);
						System.out.print("Inpaint       [");
						pq_counter=0;
						PQ.add(new Pair(p.key-0.1,p.x,p.y));
					}
					else {
						for(int b=-side/2; b<side/2; b++) {
							for(int a=-side/2; a<side/2; a++) {
								if(inpaintmap[x+a][y+b] != 0 && inpaintmap[best_patch_x+a][best_patch_y+b] == 0) {
									inpaintmap[x+a][y+b] = 0;
									edge[x+a][y+b] = 0;
									raster.getPixel(best_patch_x+a, best_patch_y+b, patch_color);
									raster.setPixel(x+a, y+b, patch_color);
									gradient[x+a][y+b] = gradient[best_patch_x+a][best_patch_y+b];
								}
							}
						}
						
						//Create new edge
						int a,b;
						//Top
						if(y+(b=-side/2-1) >= 0) {
							for(a=-side/2; a<side/2; a++) {
								if(x+a >= 0 && x+a < w)
									if(inpaintmap[x+a][y+b] != 0) {
										edge[x+a][y+b] = 1;
										//System.out.println(x+","+y+"-"+(x+a)+","+(x+b));
										normal[x+a][y+b+1][0] = 0;
										normal[x+a][y+b+1][1] = -1;
										data[x+a][y+b] = calData(x+a, y+b+1);
										confident[x+a][y+b] = calConfident(x+a, y+b);
										PQ.add(new Pair(confident[x+a][y+b]+data[x+a][y+b],x+a,y+b));
										pq_size++;
									}
							}
						}
						
						//Bottom
						if(y+(b=side/2) < h) {
							for(a=-side/2; a<side/2; a++) {
								if(x+a >= 0 && x+a < w)
									if(inpaintmap[x+a][y+b] != 0) {
										edge[x+a][y+b] = 1;
										normal[x+a][y+b-1][0] = 0;
										normal[x+a][y+b-1][1] = 1;
										data[x+a][y+b] = calData(x+a, y+b-1);
										confident[x+a][y+b] = calConfident(x+a, y+b);
										PQ.add(new Pair(confident[x+a][y+b]+data[x+a][y+b],x+a,y+b));
										pq_size++;
									}
							}
						}
						
						//Left
						if(x+(a=-side/2-1) >= 0) {
							for(b=-side/2; b<side/2; b++) {
								if(y+b >= 0 && y+b < h)
									if(inpaintmap[x+a][y+b] != 0) {
										edge[x+a][y+b] = 1;
										//System.out.println(x+","+y+"-"+(x+a)+","+(x+b));
										normal[x+a+1][y+b][0] = -1;
										normal[x+a+1][y+b][1] = 0;
										data[x+a][y+b] = calData(x+a+1, y+b);
										confident[x+a][y+b] = calConfident(x+a, y+b);
										PQ.add(new Pair(confident[x+a][y+b]+data[x+a][y+b],x+a,y+b));
										pq_size++;
									}
							}
						}
						
						//Right
						if(x+(a=side/2) < w) {
							for(b=-side/2; b<side/2; b++) {
								if(y+b >= 0 && y+b < h)
									if(inpaintmap[x+a][y+b] != 0) {
										edge[x+a][y+b] = 1;
										//System.out.println(x+","+y+"-"+(x+a)+","+(x+b));
										normal[x+a-1][y+b][0] = 1;
										normal[x+a-1][y+b][1] = 0;
										data[x+a][y+b] = calData(x+a-1, y+b);
										confident[x+a][y+b] = calConfident(x+a, y+b);
										PQ.add(new Pair(confident[x+a][y+b]+data[x+a][y+b],x+a,y+b));
										pq_size++;
									}
							}
						}
					
					//System.out.println(" with "+best_patch_x+","+best_patch_y+" ERR "+best_patch_error);
					
					//raster.setPixel(x, y, black);
					}
				}
				
				/*if(inpaintmap[x][y] == 1) {
					for(int k=0;k<3;k++) {
						new_color[k] = 0;
					}
					for(int i=-1; i<=1; i++) {
						for(int j=-1; j<=1; j++) {
							raster.getPixel(x+i, y+j, color);
							if(i!=0 || j!=0) {
								for(int k=0;k<3;k++) {
									new_color[k] += 0.125*color[k];
								}
			                }
							raster.setPixel(x+i, y+j, white);
						}
					}
				}*/
			
		}
		System.out.println("] DONE");
	
		for(int y=side/2; y<h-side/2; y++) {
			for(int x=side/2; x<w-side/2; x++) {
			}
		}
	}
	
	private static int squareError(int a[], int b[]) {
		return (b[0]-a[0])*(b[0]-a[0])+(b[1]-a[1])*(b[1]-a[1])+(b[2]-a[2])*(b[2]-a[2]);
	}
	
	private static BufferedImage overlayText(BufferedImage old, ArrayList<String> text) {
        int w2 = old.getWidth();
        int h2 = old.getHeight();
        BufferedImage img2 = new BufferedImage(
            w2, h2, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img2.createGraphics();
        g2d.drawImage(old, 0, 0, null);
        g2d.setPaint(Color.BLACK);
        
        if(line.size() < text.size()) {
        	//Naive version
	        int size = (border.bottom-border.top)/text.size();
	        int font_size = size;
	        int x = border.left; // Left 
	        int y = border.top; // Top
	        while(true) {
		        g2d.setFont(new Font("Serif", Font.PLAIN, font_size));
		        FontMetrics fm = g2d.getFontMetrics();
		        System.out.println(size+","+fm.getHeight());
		        if(fm.getHeight() <= size+1) {
		        	y -= fm.getAscent()/2;//2*fm.getLeading(); //(+fm.getDescent())/4;
		        	break;
		        }
		        font_size -= (fm.getHeight()-size)/2;
	        }
	        
	        
	        for(String s : text) {
	            
	            //img2.getWidth() - fm.stringWidth(s) - 5;
	            y += size;
	            g2d.drawString(s, x, y);
	        }
        }
        else {
        	if(line.size() > text.size()) {
        		int eliminate =  line.size() - text.size();
        		
        		//Eliminated completely outside block
        		ArrayList<int[]> dist = new ArrayList<int[]>();
        		
        		int is_new_dist = 0;
        		
        		//Loop over all text lines
        		for(int i=0; i<line.size(); i++) {
        			LineElem each_line = line.get(i);
        			//System.out.println("Line top"+each_line[0]+" middle"+each_line[1]+" bottom"+each_line[2]);
        			//System.out.print(border[2]+" "+border[3]);
        			int cur_dist = 0;
        			is_new_dist = 0;
        			if(each_line.end < border.top) { // Line Bottom < Top Border
        				cur_dist = border.top-each_line.end;
        				is_new_dist = 1;
        			}
        			else if(each_line.begin > border.bottom) { // Line Top > Bottom Border
        				cur_dist = each_line.begin-border.bottom;
        				is_new_dist = 1;
        			}
        			
        			if(is_new_dist == 1) {
        				// Save highest distance between text line to border into max heap
        				int j;
        				for(j=0; j<dist.size() && j<eliminate; j++) {
        					if(cur_dist > dist.get(j)[0])
        						break;
        				}
        				if(j<eliminate) {
        					int new_dist[] = new int[2];
        					new_dist[0] = cur_dist;
        					new_dist[1] = i;
        					dist.add(j,new_dist);
        					
        				}
        			}
        		}
        		
        		//Mark removed line and then remove
        		for(int i=0; i<eliminate && i<dist.size(); i++) {
        			line.get(dist.get(i)[1]).begin = -1;
        		}
        		for(int i=0; i<line.size(); i++) {
        			if(line.get(i).begin == -1) {
        				line.remove(i);
        				i--;
        			}
        		}
        		
        		if(line.size() > text.size()) {
        			eliminate =  line.size() - text.size();
        			//Another process of removing
        		}
        	}
        	//Calculate average font size
        	/*int font_size = 0;
        	for(int[] new_line : line) {
        		font_size+= (new_line[2] - new_line[0]);
        	}
        	font_size /= line.size();*/
        	
        	//Calculate median font size
        	int[] font_size_sorted = new int[line.size()];
        	for(int i=0; i<line.size(); i++) {
        		LineElem each_line = line.get(i);
        		font_size_sorted[i] = (each_line.end - each_line.begin);
        	}
        	Arrays.sort(font_size_sorted);

        	int med = (int) (line.size()/2);
        	int font_size = font_size_sorted[med];
	        
	        g2d.setFont(new Font("Serif", Font.PLAIN, font_size));
	        FontMetrics fm = g2d.getFontMetrics();
	        System.out.println(font_size+","+fm.getHeight());
		    
        	for(int i=0; i< text.size(); i++) { //String s : text) {
        		String s = text.get(i);
        		LineElem each_line = line.get(i);
	            int y =  (each_line.begin+each_line.end+font_size/2)/2;
	            
	            int left = border.left;
	            int right = border.right;
	            int score = 0;
	            while(score < mark_width) {
	            	for(int j = each_line.begin; j<each_line.end; j++)
	            		score += pixelmarkmap[left][j];
	            	left++;
	            }
	            
	            score = 0;
	            while(score < mark_width) {
	            	right--;
	            	for(int j = each_line.begin; j<each_line.end; j++)
	            		score += pixelmarkmap[right][j];
	            }
	            
	            int total_width = right-left;
	            int occupied_width = 0;
	            int word_count = 0;
	            String word_list[] = s.split(" ");
	            //Remove empty word
	            for(String word : word_list) {
	            	if(!word.equals(""))
	            		word_count++;
	            }
	            
	            
	            //find_word_space(i,word_list);
	            
	            // Fully distributed
	            
	            for(String word : word_list) {
	            	occupied_width += fm.stringWidth(word);
	            }
	            int space_width = (total_width-occupied_width)/(word_count-1);
	            int x = left;
	            for(String word : word_list) {
	            	if(!word.equals("")) {
	            		g2d.drawString(word, x, y);
	            		x += fm.stringWidth(word) + space_width;
	            	}
	            }
	        }
        }
        
        
        
        g2d.dispose();
        return img2;
    }
}
