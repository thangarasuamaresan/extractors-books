
package bookpreviewextractor;

import imagepreviewpyramidextractor.DziMetaData;
import imagepreviewpyramidextractor.ImagePreviewPyramidExtractor;
import imagepreviewpyramidextractor.ImagePreviewPyramidExtractorService;
import imageutils.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;
import metadata.PreviewMetadata;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * @author Theerasit Issaranon
 *
 */

public class BookPreviewExtractorService extends ExtractorService {
	private static Log log = LogFactory.getLog(BookPreviewExtractorService.class);
	private ExtractionJob job = null;

	private static final String XMLNS = "http://schemas.ncsa.illinois.edu/Medici/book";
	private static final String CONVERT = "convert";
	private static final String PREVIEW_WIDTH = "600";
	private static final String PREVIEW_HEIGHT = "600";
	
	public BookPreviewExtractorService(
			BookPreviewExtractor bookPreviewExtractor) {
		super(bookPreviewExtractor);
	}	

	public BookMetaData processJob(ExtractionJob receivedMsg) throws Exception{
				
		log.info("Downloading book with ID "+ receivedMsg.getIntermediateId() +" from " + receivedMsg.getHost());
		callingExtractor.sendStatus(receivedMsg.getId(), callingExtractor.getClass().getSimpleName(), "Downloading book file.", log);
		
		job = receivedMsg;			 	
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getIntermediateId()+"?key="+playserverKey);		
		HttpResponse fileResponse = httpclient.execute(httpGet);
		log.info(fileResponse.getStatusLine());
		if(fileResponse.getStatusLine().toString().indexOf("200") == -1){
			throw new IOException("File not found.");
		}
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9);
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")), fileName.substring(fileName.lastIndexOf(".")));
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();
		
		EntityUtils.consume(fileEntity);
		
		log.info("Download complete. Initiating book metadata extraction");
		BookMetaData bookMetaData = null;
		try{
			bookMetaData = processFile(tempFile);
		}catch(Exception e){
			tempFile.delete();
			throw e;
		}
				
		tempFile.delete();
		
		return bookMetaData;				
	}
	
	protected BookMetaData processFile(File tempFile) throws Exception{
		if (tempFile == null) {
		  return null;
		}
		
		final ArrayList<InpaintFile> file;
		    
		//extracting zip file
		log.info("Extracting Zip file " + tempFile.getName() + " with Inpaint.");
		callingExtractor.sendStatus(job.getId(), callingExtractor.getClass().getSimpleName(), "Extracting Zip file with Inpaint.", log);
		try {
			file = InpaintUtils.extractZip(tempFile);
		} catch(Throwable thr) {
			thr.printStackTrace();
			log.warn(String.format("CANNOT EXTRACT ZIP FILE %s.", tempFile.getName() ), thr);
			throw thr;
		}
		log.info("Extracted.");
		
		int page = file.size();
		
		bookXML bookxml = null;
		try{
			bookxml = new bookXML(page);
			
		    log.info("Starting Inpaint Extractor");
		    callingExtractor.sendStatus(job.getId(), callingExtractor.getClass().getSimpleName(), 
		    		"Extracting and generating previews for pages from Inpaint book with Inpaint.", log);
	        
	        for(int i=0; i<page; i++) {
	        	log.info("File "+i);
	        
	        	UploadImage baseUploadImage = new UploadImage(file.get(i).image);
	        	
	        	int bgc[] = new int[3];
	        	bgc = InpaintUtils.inpaint(file.get(i));
	        	String bgcolor = String.format("#%02x%02x%02x", bgc[0], bgc[1], bgc[2]);
	        	
	        	UploadImage overlayUploadImage = new UploadImage(file.get(i).text_image);
	        	
	        	ImagePreviewPyramidExtractorService basePyramid = new ImagePreviewPyramidExtractorService(callingExtractor);
	        	DziMetaData baseDzi = basePyramid.processJob(job, file.get(i).image);
	        	
	        	ImagePreviewPyramidExtractorService overlayPyramid = new ImagePreviewPyramidExtractorService(callingExtractor);
	        	DziMetaData overlayDzi = overlayPyramid.processJob(job, file.get(i).text_image);
	        	
	        	bookxml.addPage(baseUploadImage.getId(), overlayUploadImage.getId(), baseDzi.getId(), overlayDzi.getId(), bgcolor);
		        
	        }
		}catch(Exception e){
			//Remove extracted temp file
	        for(int i=0; i<page; i++) {
	        	file.get(i).text.delete();
	        	file.get(i).image.delete();
	        	file.get(i).text_image.delete();
	        }
	        throw e;
		}
        
        //Remove extracted temp file
        for(int i=0; i<page; i++) {
        	file.get(i).text.delete();
        	file.get(i).image.delete();
        	file.get(i).text_image.delete();
        }
        
        log.info("Finished Inpaint Extractor");
        
        File book = bookxml.createBook();
        BookMetaData bookMetaData = null;
        try{
	        String bookId = bookxml.uploadBook(book);       
	        bookMetaData = new BookMetaData(bookId, book.getName(), new Long(book.length()).toString(), Integer.toString(page));        
	        book.delete();
        }catch(Exception e){
        	book.delete();
        	throw e;
        }
        
		return bookMetaData;

	}
	
	class pageXML {
		public String baseImage;
		public String overlayImage;
		public String baseZoom;
		public String overlayZoom;
		public String bgcolor;
		
		public pageXML(String baseImage, String overlayImage, String baseZoom, String overlayZoom, String bgcolor) {
			this.baseImage = baseImage;
			this.overlayImage = overlayImage;
			this.baseZoom = baseZoom;
			this.overlayZoom = overlayZoom;
			this.bgcolor = bgcolor;
		}
	}
	
	class bookXML {
		public Document doc;
		public Element bookElement;
		public ArrayList<pageXML> pagexml;
		
		public bookXML(int page) {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = null;
			try {
				docBuilder = docFactory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
			
			doc = docBuilder.newDocument();
			bookElement = doc.createElement("book");
			doc.appendChild(bookElement);
			bookElement.setAttribute("page", Integer.toString(page));
			bookElement.setAttribute("xmlns", XMLNS);
			
			pagexml = new ArrayList<pageXML>();
		}
		
		private void addPage(String baseImage, String overlayImage, String baseZoom, String overlayZoom) {
			addPage(baseImage, overlayImage, baseZoom, overlayZoom, "#fff");
		}
		private void addPage(String baseImage, String overlayImage, String baseZoom, String overlayZoom, String bgcolor) {
			Element pageElement = doc.createElement("page");
			bookElement.appendChild(pageElement);
			pageElement.setAttribute("width", Integer.toString(800));
			pageElement.setAttribute("Height", Integer.toString(600));
			pageElement.setAttribute("bgcolor", bgcolor);
			
			Element magazineElement = doc.createElement("magazine");
			pageElement.appendChild(magazineElement);
			magazineElement.setAttribute("base", baseImage);
			magazineElement.setAttribute("overlay", overlayImage);
			
			Element zoomElement = doc.createElement("zoom");
			pageElement.appendChild(zoomElement);
			zoomElement.setAttribute("base", baseZoom);
			zoomElement.setAttribute("overlay", overlayZoom);
		
			pageXML thispage = new pageXML(baseImage, overlayImage, baseZoom, overlayZoom, bgcolor);
			pagexml.add(thispage);
		}
		
		private File createBook() throws ParserConfigurationException, TransformerException, IOException {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			File book = File.createTempFile(job.getId(), ".xml");
			StreamResult result = new StreamResult(book);
			
			transformer.transform(source, result);
			
			return book;
		}
		
		private String uploadBook(File book) throws ClientProtocolException, IOException {
			
			log.info("Uploading book XML");
			
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(job.getHost() +"api/previews?key="+playserverKey);
			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			entity.addPart("File", new FileBody(book,"application/xml"));
	        httpPost.setEntity(entity);          
			HttpResponse dziUploadResponse = httpclient.execute(httpPost);
			
			System.out.println(dziUploadResponse.getStatusLine());						
			HttpEntity idEntity = dziUploadResponse.getEntity();
			String json = EntityUtils.toString(idEntity);
			String bookId = json.substring(json.indexOf("\"id\":\"")+6,json.lastIndexOf("\"")-(json.indexOf("\"id\":\"")+6)+7);
			System.out.println("bookId: "+ bookId);
			
			log.info("Uploaded");
						
			EntityUtils.consume(idEntity); 
			EntityUtils.consume(entity);
	    	
	    	return bookId;
		}
	}
	
	class UploadImage {
		private File imageFile = null;
		private String imageFormat = "";
        private String imageId = "";
        
        public String getId() {
        	return imageId;
        }
        
        public UploadImage(File imageFile) throws IOException {//BufferedImage img, String imageName, String imageFormat) {
        	this.imageFile = imageFile;
        	this.imageFormat = imageFile.getName().substring(imageFile.getName().lastIndexOf(".")+1);
        	
            try {
                /*Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(imageFormat);
                ImageWriter writer = iter.next();
                ImageWriteParam iwp = writer.getDefaultWriteParam();
                if (imageFormat.equals("jpg")) { //$NON-NLS-1$
                    iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                    iwp.setCompressionQuality(0.95f);
                }
                
                imageFile = File.createTempFile(imageName , "." + imageFormat );
                FileImageOutputStream output = new FileImageOutputStream(imageFile);
                try {
                    writer.setOutput(output);
                    IIOImage image = new IIOImage(img, null, null);
                    writer.write(null, image, iwp);
                } finally {
                    output.close();
                }*/
            	log.info("Creating Preview Image " + imageFile.getName());
            	
            	File previewFile = createPreview();
            	try{
	                log.info("Uploading Image " + imageFile.getName());
	    			
	    			DefaultHttpClient httpclient = new DefaultHttpClient();
	    			HttpPost httpPost = new HttpPost(job.getHost() +"api/previews?key="+playserverKey);
	    			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
	    			entity.addPart("File", new FileBody(previewFile, imageFormat));
	    	        httpPost.setEntity(entity);          
	    			HttpResponse imgUploadResponse = httpclient.execute(httpPost);
	    			
	    			System.out.println(imgUploadResponse.getStatusLine());						
	    			HttpEntity idEntity = imgUploadResponse.getEntity();
	    			String json = EntityUtils.toString(idEntity);
	    			this.imageId = json.substring(json.indexOf("\"id\":\"")+6,json.lastIndexOf("\"")-(json.indexOf("\"id\":\"")+6)+7);
	    			System.out.println("image id: "+ imageId);
	    			
	    			EntityUtils.consume(idEntity); 
	    			EntityUtils.consume(entity);
            	}catch(Exception e){
            		previewFile.delete();
            		throw e;
            	}
    			
    			previewFile.delete();
    			log.info(imageFile.getName()+" Uploaded");
    			
            } catch (Throwable thr) {
                log.error(thr);
            }
        }
		
        /* REQUIRE External_Utils*/
        private File createPreview() throws IOException {
		 	File previewFile = File.createTempFile("preview", ".png");
	        previewFile.delete();
	        
	        ArrayList<String> cmd = new ArrayList<String>();
	        cmd.add(CONVERT);
	        cmd.add(imageFile.getAbsolutePath());
	        cmd.add("-thumbnail");
	        cmd.add(PREVIEW_WIDTH + "x" + PREVIEW_HEIGHT);
	        
	        cmd.add(previewFile.getAbsolutePath());
	        String stdout = ExternalUtils.runProcess(cmd);
	        log.debug(stdout);
	        if (!previewFile.exists()) {
	            throw (new IOException("No output generated (maybe format not supported)."));
	        }
	        
	        return previewFile;
	        /*Dimension wh = identify(png);
	        
	        PreviewImageBean pib = new PreviewImageBean();
	        pib.setSize(png.length());
	        pib.setHeight(w);
	        pib.setWidth(h);
	        
	        CETBeans.writeBlob(getBeansession(), Resource.uriRef(pib.getUri()), png);
	        png.delete();
	        
	        return pib;*/
        }
        
		private void associateImage(String bookId) throws ClientProtocolException, IOException {
        	log.info("Associating Image " + imageId);
	    
        	BufferedImage image = null;       
 	        image = ImageIO.read(imageFile);
	        
 	        ObjectMapper mapper= new ObjectMapper();
 	        String JSONString = mapper.writeValueAsString(new PreviewMetadata(imageFormat,
 	        		new Integer(image.getHeight()).toString(),
 	        		new Integer(image.getWidth()).toString()));
 	        
 	        DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(job.getHost() +"api/files/" + bookId
					+ "/previews/" + imageId + "?key=" + playserverKey);
			httpPost.addHeader("content-type","application/json");
			StringEntity theJSON = new StringEntity(JSONString);
			
            httpPost.setEntity(theJSON);			
			HttpResponse imgUploadResponse = httpclient.execute(httpPost);
			
			log.info(imgUploadResponse.getStatusLine());
			
			EntityUtils.consume(theJSON);
			
		}
	}
}

