package imagepreviewpyramidextractor;

public class DziMetaData {

	private final String id;
	private final String name;
	private final String length;
	
	public DziMetaData(String id, String name, String length) {
		super();
		this.id = id;
		this.name = name;
		this.length = length;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLength() {
		return length;
	}
	
	
}
