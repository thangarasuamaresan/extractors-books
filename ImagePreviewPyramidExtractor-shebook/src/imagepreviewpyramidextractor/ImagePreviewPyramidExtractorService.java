package imagepreviewpyramidextractor;

import imageutils.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import medici2extractor.ExtractionJob;
import medici2extractor.ExtractorService;
import metadata.PreviewMetadata;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ImagePreviewPyramidExtractorService extends ExtractorService {
	private static Log log = LogFactory.getLog(ImagePreviewPyramidExtractorService.class);
	private static final String KEY = "r1ek3rs";
	private static final int PYRAMID_THREADS = 10;
	private static final long EXEC_MAX_WAIT = 604800;
	private static Long minFileSize = Long.parseLong("0");
	private ExtractionJob job = null;
	
	private static final String SERVER_FORMAT = "Default";
	private static final int TILE_SIZE = 256;
	private static final int OVERLAP = 1;
	private static final String XMLNS = "http://schemas.microsoft.com/deepzoom/2009";
	
	public static void setMinSize(Integer minSize) {
		ImagePreviewPyramidExtractorService.minFileSize = Long.parseLong(minSize.toString()) * 1048576;
	}

	public DziMetaData processJob(ExtractionJob receivedMsg) throws Exception{
		
		//If not a very big image, do nothing (image will have no pyramid-based preview and thus will be rendered as a regular image).
		if(Long.parseLong(receivedMsg.getFileSize()) <= ImagePreviewPyramidExtractorService.minFileSize){
			return null;
		}
		
		log.info("Downloading image file with ID "+ receivedMsg.getIntermediateId() +" from " + receivedMsg.getHost());
		job = receivedMsg;			 	
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(receivedMsg.getHost() +"api/files/"+ receivedMsg.getIntermediateId()+"?key="+KEY);		
		HttpResponse fileResponse = httpclient.execute(httpGet);
		log.info(fileResponse.getStatusLine());
		HttpEntity fileEntity = fileResponse.getEntity();
		InputStream fileIs = fileEntity.getContent();

		Header[] hdrs = fileResponse.getHeaders("content-disposition");
		String contentDisp = hdrs[0].toString();

		String fileName = contentDisp.substring(contentDisp.indexOf("filename=")+9);
		File tempFile = File.createTempFile(fileName.substring(0, fileName.lastIndexOf(".")), fileName.substring(fileName.lastIndexOf(".")));
		OutputStream fileOs = new FileOutputStream(tempFile);		
		IOUtils.copy(fileIs,fileOs);
		fileIs.close();
		fileOs.close();
		
		EntityUtils.consume(fileEntity);
		
		log.info("Download complete. Initiating pyramid extraction");
		
		DziMetaData dziMetaData = processFile(tempFile);
		tempFile.delete();
		
		return dziMetaData;				
	}
	
	public DziMetaData processJob(ExtractionJob job, File tempFile) throws Exception{
		this.job = job;
		return processFile(tempFile);
	}
	
	protected DziMetaData processFile(File tempFile) throws Exception{
			
		BufferedImage img = ImageIO.read(tempFile);
						
		String imageformat = tempFile.getName().substring(tempFile.getName().lastIndexOf(".")+1);		
		boolean smooth = true;
		
		// ImageIO wants jpeg with samplesize of 8 or less
		if ((imageformat.equals("jpeg") || imageformat.equals("jpg")) && img.getSampleModel().getSampleSize(0) > 8) { //$NON-NLS-1$
            img = ImageUtils.scaleImage(img, img.getWidth(), img.getHeight(), false, false);
        }
		
		// get the width/height and maximum
        int width = img.getWidth();
        int height = img.getHeight();
        
        File dzi = createDzi(imageformat, width, height);
        String dziId = uploadDzi(dzi);
        
        DziMetaData dziMetaData = new DziMetaData(dziId, dzi.getName(), new Long(dzi.length()).toString());
        
        //Delete temporary DZI file
        dzi.delete();

        // compute number of levels until 1x1 pixel
        int maxlevel = (int) Math.ceil(Math.log(Math.max(width, height)) / Math.log(2));
		       
        // now process image
        // level 0 is 1x1 pix, level maxlevel is full image
        for (int l = maxlevel; l >= 0; l-- ) {
            log.debug(String.format("Creating level %d.", l));

            // execution service
            ExecutorService exec = Executors.newFixedThreadPool(PYRAMID_THREADS);

            // compute number of rows & columns
            int columns = (int) Math.ceil(width / TILE_SIZE);
            int rows = (int) Math.ceil(height / TILE_SIZE);

            for (int r = 0; r <= rows; r++ ) {
                // compute the tilesize vertical
                int y = r * TILE_SIZE;
                int th = TILE_SIZE + OVERLAP;
                if (r != 0) {
                    y -= OVERLAP;
                    th += OVERLAP;
                }
                th = Math.min(th, height - y);

                // write all images in a row
                for (int c = 0; c <= columns; c++ ) {
                    // compute the tilesize horizontally
                    int x = (c * TILE_SIZE);
                    int tw = TILE_SIZE + OVERLAP;
                    if (c != 0) {
                        x -= OVERLAP;
                        tw += OVERLAP;
                    }
                    tw = Math.min(tw, width - x);
                 // save the tile in a new thread
                    if (PYRAMID_THREADS == 1) {
                        new WriteSubImage(img, x, y, tw, th, l, c, r, imageformat, dziId).run();
                    } else {
                        exec.execute(new WriteSubImage(img, x, y, tw, th, l, c, r, imageformat, dziId));
                    }

                }
            }
            
         // wait for all threads
            if (PYRAMID_THREADS != 1) {
                exec.shutdown();
                if (!exec.awaitTermination(EXEC_MAX_WAIT, TimeUnit.SECONDS)) {
                    int count = exec.shutdownNow().size();
                    log.warn(String.format("Waited more than %d seconds, killed %d jobs.", EXEC_MAX_WAIT, count));
                }
            }

            // compute dimensions of next level
            width = (int) Math.ceil(width / 2.0);
            height = (int) Math.ceil(height / 2.0);

            // scale the image
            img = ImageUtils.scaleImage(img, width, height, false, smooth);
            if (width != img.getWidth()) {
                log.warn(String.format("WIDTH IS NOT RIGHT %d != %d", width, img.getWidth()));
                width = img.getWidth();
            }
            if (height != img.getHeight()) {
                log.warn(String.format("HEIGHT IS NOT RIGHT %d != %d", height, img.getHeight()));
                height = img.getHeight();
            }
        }
        
        return dziMetaData;
	}
	
	private File createDzi(String imageformat, int width, int height) throws ParserConfigurationException, TransformerException, IOException {
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		
		Document doc = docBuilder.newDocument();
		Element imageElement = doc.createElement("Image");
		doc.appendChild(imageElement);
		imageElement.setAttribute("TileSize", new Integer(TILE_SIZE).toString());
		imageElement.setAttribute("Overlap", new Integer(OVERLAP).toString());
		imageElement.setAttribute("Format", imageformat);
		imageElement.setAttribute("ServerFormat", SERVER_FORMAT);
		imageElement.setAttribute("xmlns", XMLNS);
		
		Element sizeElement = doc.createElement("Size");
		imageElement.appendChild(sizeElement);
		sizeElement.setAttribute("Width", new Integer(width).toString());
		sizeElement.setAttribute("Height", new Integer(height).toString());
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		File dzi = File.createTempFile(job.getId(), ".xml");
		StreamResult result = new StreamResult(dzi);
		
		transformer.transform(source, result);
		
		return dzi;
	}
	
private String uploadDzi(File dzi) throws ClientProtocolException, IOException {
		
		log.info("Uploading gigaimage DZI");
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(job.getHost() +"api/previews?key="+KEY);
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addPart("File", new FileBody(dzi,"application/xml"));
        httpPost.setEntity(entity);          
		HttpResponse dziUploadResponse = httpclient.execute(httpPost);
		
		System.out.println(dziUploadResponse.getStatusLine());						
		HttpEntity idEntity = dziUploadResponse.getEntity();
		String json = EntityUtils.toString(idEntity);
		String dziId = json.substring(json.indexOf("\"id\":\"")+6,json.lastIndexOf("\"")-(json.indexOf("\"id\":\"")+6)+7);
		System.out.println("prev id: "+ dziId);
		
		log.info("Gigaimage DZI uploaded");
					
		EntityUtils.consume(idEntity); 
		EntityUtils.consume(entity);
    	
    	return dziId;
	}

	class WriteSubImage implements Runnable {

        private final BufferedImage img;
        private final int           x;
        private final int           y;
        private final int           w;
        private final int           h;
        private final int           l;
        private final int           c;
        private final int           r;
        private final String        imageformat;
        private final String        dziId;

        public WriteSubImage(BufferedImage img, int x, int y, int w, int h, int l, int c, int r, String imageformat, String dziId) {
            this.img = img;
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.l = l;
            this.c = c;
            this.r = r;
            this.imageformat = imageformat;
            this.dziId = dziId;
        }
        
        public void run() {
            try {
                Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(imageformat);
                ImageWriter writer = iter.next();
                ImageWriteParam iwp = writer.getDefaultWriteParam();
                if (imageformat.equals("jpg")) { //$NON-NLS-1$
                    iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                    iwp.setCompressionQuality(0.95f);
                }

                // images are named col_row
                BufferedImage subimg = img.getSubimage(x, y, w, h);
                File tileFile = File.createTempFile(c + "_" + r + "_"  , "." + imageformat );
                FileImageOutputStream output = new FileImageOutputStream(tileFile);
                try {
                    writer.setOutput(output);
                    IIOImage image = new IIOImage(subimg, null, null);
                    writer.write(null, image, iwp);
                } finally {
                    output.close();
                }
                
                MagicMatch match =Magic.getMagicMatch(tileFile, true, true);
                
                String tileId = uploadTile(tileFile, match.getMimeType());
                
                BufferedImage image = null;       
    	        image = ImageIO.read(tileFile);
   	        
    	        ObjectMapper mapper= new ObjectMapper();
    	        String JSONString = mapper.writeValueAsString(new PreviewMetadata(
    	        		match.getMimeType(),new Integer(image.getHeight()).toString(),
    	        		new Integer(image.getWidth()).toString()));
    	        
    	        associateTile(tileId, l, JSONString);

                // delete the temp file
                tileFile.delete();

            } catch (Throwable thr) {
                log.error(thr);
            }
        }
        
        private void associateTile(String tileId, int l,
				String JSONString) throws ClientProtocolException, IOException {
			
        	log.info("Associating gigaimage file tile with file");
	    	
	    	DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(job.getHost() +"api/previews/" + dziId
					+ "/tiles/" + tileId + "/" + l + "?key=" + KEY);
			httpPost.addHeader("content-type","application/json");
			StringEntity theJSON = new StringEntity(JSONString);
			
            httpPost.setEntity(theJSON);			
			HttpResponse tileUploadResponse = httpclient.execute(httpPost);
			
			log.info(tileUploadResponse.getStatusLine());
			
			EntityUtils.consume(theJSON);
		}

		private String uploadTile(File tileFile, String mimeType) throws IOException{
        	
        	log.info("Uploading gigaimage file tile " + tileFile.getName());
			
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(job.getHost() +"api/tiles?key="+KEY);
			MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			entity.addPart("File", new FileBody(tileFile,mimeType));
            httpPost.setEntity(entity);          
			HttpResponse tileUploadResponse = httpclient.execute(httpPost);
			
			log.info(tileUploadResponse.getStatusLine());						
			HttpEntity idEntity = tileUploadResponse.getEntity();
			String json = EntityUtils.toString(idEntity);
			String tileId = json.substring(json.indexOf("\"id\":\"")+6,json.lastIndexOf("\"")-(json.indexOf("\"id\":\"")+6)+7);
			log.info("tile id: "+ tileId);
			
			log.info("Gigaimage file tile uploaded");
						
			EntityUtils.consume(idEntity);
			EntityUtils.consume(entity);
	    	
	    	return tileId;
        }
    }
	
}
