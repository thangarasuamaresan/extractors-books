package imagepreviewpyramidextractor;

import java.io.IOException;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;
import metadata.FileMetadata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class ImagePreviewPyramidExtractor extends Extractor {

	private static final String QUEUE_NAME = "medici_image_pyramid";
	private static final String EXCHANGE_NAME = "medici";
	private static final String CONSUMER_TAG = "medici_image_pyramid";
	private static final boolean DURABLE = true;
	private static final boolean EXCLUSIVE = false;
	private static final boolean AUTO_DELETE = false;
	
	private static String serverAddr = "";
	private static String messageReceived = "";
	
	private static Log log = LogFactory.getLog(ImagePreviewPyramidExtractor.class);
	
	public static void main(String[] args)  {
        
        if (args.length < 4){ 
			System.out.println("Input RabbitMQ server address, followed by RabbitMQ username, RabbitMQ password and minimum size of gigaimages(in megabytes).");
			return;
		}   
            serverAddr = args[0];
            ImagePreviewPyramidExtractorService.setMinSize(Integer.parseInt(args[3]));
            new ImagePreviewPyramidExtractor().startExtractor(args[1], args[2]);	        
    }
	
	 protected void startExtractor(String rabbitMQUsername, String rabbitMQpassword) {
		 try{			 
				//Open channel and declare exchange and consumer
				ConnectionFactory factory = new ConnectionFactory();
				factory.setHost(serverAddr);
				factory.setUsername(rabbitMQUsername);
				factory.setPassword(rabbitMQpassword);
				Connection connection = factory.newConnection();
		    
				final Channel channel = connection.createChannel();
				channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
		
				channel.queueDeclare(QUEUE_NAME,DURABLE,EXCLUSIVE,AUTO_DELETE,null);
				channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.file.image.jpeg.#");
				channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.file.image.jpg.#");
				channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.file.image.png.#");
				channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.file.image.tiff.#");

				// create listener
		        channel.basicConsume(QUEUE_NAME, false, CONSUMER_TAG, new DefaultConsumer(channel) {
		            @Override
		            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		                messageReceived = new String(body);
		                long deliveryTag = envelope.getDeliveryTag();
		                // (process the message dcomponents here ...)
		                System.out.println(" {x} Received '" + messageReceived + "'");
			            processMessageReceived();
			            System.out.println(" [x] Done");
		                channel.basicAck(deliveryTag, false);
		            }
		        });
				
		        // start listening
		        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
		        while (true) {
		           Thread.sleep(1000);
		        }
			}
	        catch(Exception e){
	        	e.printStackTrace();
	            System.exit(1);
	        }		 
	    }
	 
	 protected boolean processMessageReceived(){
	    	try{
	    		try {	    			
	    			ImagePreviewPyramidExtractorService extrServ = new ImagePreviewPyramidExtractorService();
	    			jobReceived = getRepresentation(messageReceived, ExtractionJob.class);
	    			DziMetaData dziMetaData = extrServ.processJob(jobReceived);
	    			
	    			if (dziMetaData == null){
	    				log.info("Image too small to need pyramid creation. Aborted pyramid creation.");
	    				return true;
	    			}

	    			log.info("Pyramid extraction and association complete. Returning DZI file metadata.");

	    	        String JSONString = mapper.writeValueAsString(new FileMetadata(dziMetaData.getName(), dziMetaData.getLength()));
	    	        
	    	        associatePreview(dziMetaData.getId(),JSONString, log);
	    	        	    			
				} catch (IOException ioe) {
					log.error("The message could not be parsed into an ExtractionJob", ioe);
				}        
	    	}catch(Exception e){
	    		e.printStackTrace();
	            System.exit(1);
	    	}
			return false;
	    } 
}














