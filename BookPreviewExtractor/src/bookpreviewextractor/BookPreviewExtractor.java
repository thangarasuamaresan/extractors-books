package bookpreviewextractor;

import java.io.IOException;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;
import metadata.FileMetadata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

/**
 * @author Theerasit Issaranon
 * 
 */

public class BookPreviewExtractor extends Extractor {

	private static final String QUEUE_NAME = "medici_book";
	private static final String EXCHANGE_NAME = "medici";
	private static final String CONSUMER_TAG = "medici_book";
	private static final boolean DURABLE = true;
	private static final boolean EXCLUSIVE = false;
	private static final boolean AUTO_DELETE = false;

	private static String serverAddr = "";
	private static String messageReceived = "";

	private static Log log = LogFactory.getLog(BookPreviewExtractor.class);

	public static void main(String[] args) {

		if (args.length < 4) {
			System.out.println("Input RabbitMQ server address, followed by RabbitMQ username, RabbitMQ password and minimum size of gigaimages(in megabytes).");
			return;
		}
		serverAddr = args[0];
		BookPreviewExtractorService.setMinSize(Integer.parseInt(args[3]));
		new BookPreviewExtractor().startExtractor(args[1], args[2]);
	}
	

	protected String getQueueName() {
		return QUEUE_NAME;
	}
	
	protected String getConsumerTag() {
		return CONSUMER_TAG;
	}
	
	protected void startExtractor(String rabbitMQUsername, String rabbitMQpassword) {
		try {
			// Open channel and declare exchange and consumer
			ConnectionFactory factory = new ConnectionFactory();
			factory.setHost(serverAddr);
			factory.setUsername(rabbitMQUsername);
			factory.setPassword(rabbitMQpassword);
			Connection connection = factory.newConnection();

			final Channel channel = connection.createChannel();
			channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);

			channel.queueDeclare(getQueueName(), DURABLE, EXCLUSIVE, AUTO_DELETE, null);
			queueBind(channel);

			// create listener
			channel.basicConsume(getQueueName(), false, getConsumerTag(), new DefaultConsumer(channel) {
				@Override
				public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
					messageReceived = new String(body);
					long deliveryTag = envelope.getDeliveryTag();
					// (process the message dcomponents here ...)
					System.out.println(" {x} Received '" + messageReceived + "'");
					processMessageReceived();
					System.out.println(" [x] Done");
					channel.basicAck(deliveryTag, false);
				}
			});

			// start listening
			System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
			while (true) {
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	protected void queueBind(final Channel channel) throws IOException {
		channel.queueBind(getQueueName(), EXCHANGE_NAME, "*.file.application.zip.#");
		channel.queueBind(getQueueName(), EXCHANGE_NAME, "*.file.application.x-zip-compressed.#");
	}

	protected boolean processMessageReceived() {
		try {
			try {
				BookPreviewExtractorService extrServ = new BookPreviewExtractorService(this);
				System.out.print(messageReceived);
				jobReceived = getRepresentation(messageReceived, ExtractionJob.class);
				BookMetaData bookMetaData = extrServ.processJob(jobReceived);

				if (bookMetaData == null) {
					log.info("Image too small to need pyramid creation. Aborted pyramid creation.");
					return true;
				}

				log.info("Book extraction and association complete. Returning Book file metadata.");

				String JSONString = mapper.writeValueAsString(new FileMetadata(bookMetaData.getName(), bookMetaData.getLength()));

				associatePreview(bookMetaData.getId(), JSONString, log);

			} catch (IOException ioe) {
				log.error("The message could not be parsed into an ExtractionJob", ioe);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return false;
	}
}
