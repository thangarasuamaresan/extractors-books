Scen. II. The Convent of Pleasure. 3
Serv. Heaven forbid.
L. Happy. Nay, Heaven doth not only allow of it,
but commands it; for we are commanded to give to
those that want.
Enter Madam Mediator to the Lady Happy.
Mediat. Surely, Madam, you do but talk, and in-
tend not to go where you say.
L. Happy. Yes, truly, my Words and Intentions
go even together.
Mediat. But surely you will not incloyster your
self, as you say.
L. Happy. Why, what is there in the publick World
that should invite me to live in it?
Mediat. More then if you should banish your self
from it.
L. Happy. Put the case I should Marry the best of
Men, if any best there be; yet would a Marry�d life
have more crosses and sorrows then pleasure, freedom,
or happiness: nay Marriage to those that are virtuous is
a greater restraint then a Monastery. Or, should I take
delight in Admirers? they might gaze on my Beauty,
and praise my Wit, and I receive nothing from their
eyes, nor lips; for Words vanish as soon as spoken, and
Sights are not substantial. Besides, I should lose more
of my Reputation by their Visits, then gain by their
Praises. Or, should I quit Reputation and turn Cour-
tizan, there would be more lost in my Health, then
gained by my Lovers, I should find more pain then
Plea-
