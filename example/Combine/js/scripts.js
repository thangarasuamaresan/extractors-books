$(document).ready(function(){

	new FastClick(document.body);
	
	(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener("click",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;"href"in d&&(d.href.indexOf("http")||~d.href.indexOf(e.host))&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,"standalone")
	
	if (navigator.userAgent.match(/iPad/i) != null) {
		$(window).bind('orientationchange', function(e, onready){
			if (Math.abs(window.orientation) != 90){
				$('body').addClass('portrait').append('<img id="orientNotice" src="images/orient_notice.png" alt="" width="768" height="1004" />');
			} 
			else {
				$('body').removeClass('portrait');
				$('#orientNotice').remove();
			}
		});
		$(window).trigger('orientationchange', true);
	};
	
	/* http://stackoverflow.com/questions/10357844/how-to-disable-rubber-band-in-ios-web-apps */
	
	(function registerScrolling($) {
	    var prevTouchPosition = {},
	        scrollYClass = 'scroll-y',
	        scrollXClass = 'scroll-x',
	        searchTerms = '.' + scrollYClass + ', .' + scrollXClass;
	
	    $('body').on('touchstart', function (e) {
	        var $scroll = $(e.target).closest(searchTerms),
	            targetTouch = e.originalEvent.targetTouches[0];
	
	        // Store previous touch position if within a scroll element
	        prevTouchPosition = $scroll.length ? { x: targetTouch.pageX, y: targetTouch.pageY } : {};
	    });
	
	    $('body').on('touchmove', function (e) {
	    
	        var $scroll = $(e.target).closest(searchTerms),
	            targetTouch = e.originalEvent.targetTouches[0];
	
	        if (prevTouchPosition && $scroll.length) {
	            // Set move helper and update previous touch position
	            var move = {
	                x: targetTouch.pageX - prevTouchPosition.x,
	                y: targetTouch.pageY - prevTouchPosition.y
	            };
	            //console.log(targetTouch.pageY + " and " + prevTouchPosition.y);
	            prevTouchPosition = { x: targetTouch.pageX, y: targetTouch.pageY };
	
	            // Check for scroll-y or scroll-x classes
	            if ($scroll.hasClass(scrollYClass)) {
	                var scrollHeight = $scroll[0].scrollHeight,
	                    outerHeight = $scroll.outerHeight(),
	
	                    atUpperLimit = ($scroll.scrollTop() === 0),
	                    atLowerLimit = (scrollHeight - $scroll.scrollTop() === outerHeight);
	
	                if (scrollHeight > outerHeight && Math.abs(move.y) > Math.abs(move.x)) {
	                    // If at either limit move 1px away to allow normal scroll behavior on future moves,
	                    // but stop propagation on this move to remove limit behavior bubbling up to body
	                    if (move.y > 0 && atUpperLimit) {
	                        $scroll.scrollTop(1);
	                        e.stopPropagation();
	                        e.preventDefault();
	                    } else if (move.y < 0 && atLowerLimit) {
	                        $scroll.scrollTop($scroll.scrollTop()-1);
	                        e.stopPropagation();
	                        e.preventDefault(); 
	                    }
	                    
	                    // Normal scrolling behavior passes through
	                } else {
	                    // No scrolling / adjustment when there is nothing to scroll
	                    e.preventDefault();
	                    
	                }
	            } else if ($scroll.hasClass(scrollXClass)) {
	                var scrollWidth = $scroll[0].scrollWidth,
	                    outerWidth = $scroll.outerWidth(),
	
	                    atLeftLimit = $scroll.scrollLeft() === 0,
	                    atRightLimit = scrollWidth - $scroll.scrollLeft() === outerWidth;
	
	                if (scrollWidth > outerWidth && Math.abs(move.x) > Math.abs(move.y)) {
	                    if (move.x > 0 && atLeftLimit) {
	                        $scroll.scrollLeft(1);
	                        e.stopPropagation();
	                        e.preventDefault();
	                    } else if (move.x < 0 && atRightLimit) {
	                        $scroll.scrollLeft($scroll.scrollLeft() - 1);
	                        e.stopPropagation();
	                        e.preventDefault();
	                    } 
	                    
	                    // Normal scrolling behavior passes through
	                } else {
	                    // No scrolling / adjustment when there is nothing to scroll
	                    e.preventDefault();
	                    
	                }
	            }
	  	    } else {
	            // Prevent scrolling on non-scrolling elements
	            e.preventDefault();
	        }
	    });
	})(jQuery);
	
	$("#titles > li > div h2").each(function() {
        var wordArray = $(this).text().split(" ");
        if (wordArray.length > 2) {
	        wordArray[wordArray.length-2] += "&nbsp;" + wordArray[wordArray.length-1];
	        wordArray.pop();
	        $(this).html(wordArray.join(" "));
        }
	});
	
	$("#cavendish_plays_image").turn({
		page: 2
	});
	
	$("#cavendish_plays_text").turn({
		page: 2
	});
	
	// http://jsfiddle.net/UqxQk/8/
	
	jQuery.easing.easeForCSSTransition = function(x, t, b, c, d, s) {
        return b + c;
    };
    
    var changeMode = $("#display_container").cycle({
	    fx: 'scrollHorz',
	    easing: 'easeForCSSTransition',
		speed: 500,
		timeout: 0,
		startingSlide: 0
	});
	
	//changeMode.cycle(1);
	
	$(".imageButton").click(function() {
		var selector = $(this).attr("href");
		var page = $(this).attr("data-page");
		$(selector).turn("page", page);
		changeMode.cycle(1);
		return false;
	});
	
	$(".textButton").click(function() {
		var selector = $(this).attr("href");
		var page = $(this).attr("data-page");
		$(selector).turn("page", page);
		changeMode.cycle(2);
		return false;
	});
	
	$(".overviewButton").click(function() {
		changeMode.cycle(0);
		return false;
	});
	
	$(".imageViewButton").click(function() {
		var book = "#" + $(this).parent().parent().parent().children("div").attr("id");
		var currPage = $(book).turn("page");
		var selectorArray = new Array();
		selectorArray = book.split("_");
		var newSelector = "";
		for (var i = 0; i < selectorArray.length-1; i++) {
			newSelector = newSelector + (selectorArray[i] + "_");
		}
		$(newSelector + "image").turn("page", currPage);
		changeMode.cycle(1, "fade");
		return false;
	});
	
	$(".textViewButton").click(function() {
		var book = "#" + $(this).parent().parent().parent().children("div").attr("id");
		var currPage = $(book).turn("page");
		var selectorArray = new Array();
		selectorArray = book.split("_");
		var newSelector = "";
		for (var i = 0; i < selectorArray.length-1; i++) {
			newSelector = newSelector + (selectorArray[i] + "_");
		}
		$(newSelector + "text").turn("page", currPage);
		changeMode.cycle(2, "fade");
		return false;
	});
	
	$(".prevButton").click(function() {
		var book = "#" + $(this).parent().parent().parent().children("div").attr("id");
		$(book).turn("previous");
		return false;
	});
	
	$(".nextButton").click(function() {
		var book = "#" + $(this).parent().parent().parent().children("div").attr("id");
		$(book).turn("next");
		return false;
	});
	
	$("#titles").cycle({
	    fx: 'fade',
	    easing: 'easeForCSSTransition',
		speed: 250,
		timeout: 0,
		startingSlide: 7,
		pager:  '#titleList',
		activePagerClass: 'active', 
	    pagerAnchorBuilder: function(idx, slide) {
	        return '#titleList li:eq(' + idx + ')';
	    }
	});
	
	$("#titles > li").cycle({
	    fx: 'scrollHorz',
	    easing: 'easeForCSSTransition',
		speed: 500,
		timeout: 0,
		startingSlide: 0
	});
	
	//changeMode.cycle(1);
	
	$(".viewButton").click(function() {
		var selector = $(this).closest("#titles > li");
		var view = parseInt($(this).attr("data-view"));
		selector.cycle(view);
		return false;
	});
	
	$(".closeViewButton").click(function() {
		var selector = $(this).closest("#titles > li");
		selector.cycle(0);
		return false;
	});
	
	var height;
	var menuHeight;
	var panelHeight;
	var panelPadding;
	
	$("ul.toc li").not(".header").toggle(function(){
		menuHeight = $(this).children("ul").height();
		panelHeight = $(this).parent("ul").parent("div").height();
		panelPadding = parseInt($(this).parent("ul").parent("div").css("padding-bottom"), 10);
		if (menuHeight > panelHeight) {
			height = panelHeight+panelPadding;
		} else {
			height = menuHeight+panelPadding;
		}
		$(this).children("img").attr("src", "images/disc_arrow_up.png");
		$(this).children("ul").slideDown(250);
		$(this).parent("ul").parent("div").animate({scrollTop: '+=' + height}, 250);
	}, function() {
		if (menuHeight > panelHeight) {
			height = panelHeight-panelPadding;
		} else {
			height = menuHeight-panelPadding;
		}
		$(this).children("img").attr("src", "images/disc_arrow_down.png");
		$(this).children("ul").slideUp(250);
		$(this).parent("ul").parent("div").animate({scrollTop: '-=' + height}, 250);
	});

});