package bookpreviewextractor;

import java.io.File;

import medici2extractor.ExtractionJob;
import medici2extractor.Extractor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Theerasit Issaranon
 * 
 */

public class SheBookPreviewExtractorService extends BookPreviewExtractorService {
	public SheBookPreviewExtractorService(Extractor theExtractor) {
		super(theExtractor);
	}

	protected static Log log = LogFactory.getLog(SheBookPreviewExtractorService.class);

	protected static final String XMLNS = "http://schemas.ncsa.illinois.edu/Medici/shebook";

	public BookMetaData processJob(ExtractionJob receivedMsg) throws Exception{
		return super.processJob(receivedMsg);
	}

	public boolean getIncludedText() {
		return false;
	}
}
